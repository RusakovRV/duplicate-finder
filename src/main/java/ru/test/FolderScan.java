package ru.test;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Class for directory scanning
 */
public class FolderScan {

    static final String RESULT_PATH = "results.txt";

    /**
     * @param path Root path of the directory to find all files
     */
    public static List<String> generatePathsList(String path) {
        Path start = Paths.get(path);
        try {
            return Files.walk(start).filter(f -> !f.toFile().isDirectory()).map(f -> f.toAbsolutePath().toString()).collect(Collectors.toList());
        } catch (IOException ex) {
            System.out.println("ERROR: Unable to scan directory on given path");
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * @param paths List of paths to all files in root folder
     */
    static Map<String, List<String>> runHashGeneration(List<String> paths) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Map<String, Future<HashCode>> futuresMap = paths.stream().collect(Collectors.toMap(v -> (v), v -> (executorService.submit(new HashCodeCallable(v)))));
        Map<String, List<String>> resultMap = new HashMap<>();
        futuresMap.entrySet().forEach(e -> {
            try {
                HashCode hashcode = e.getValue().get();
                if (hashcode != null) {
                    if (resultMap.containsKey(hashcode.toString())) {
                        resultMap.get(hashcode.toString()).add(e.getKey());
                    } else {
                        resultMap.put(hashcode.toString(), new ArrayList<>(Arrays.asList(e.getKey())));
                    }
                }
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
        });
        executorService.shutdown();
        return resultMap;
    }

    /**
     * @param resultMap Map with hashcode as a key and list pof paths as a value
     */
    static void printResults(Map<String, List<String>> resultMap) {
        AtomicInteger count = new AtomicInteger(0);
        StringBuilder sb = new StringBuilder();
        resultMap.entrySet().forEach(v -> {
            if (v.getValue().size() > 1) {
                sb.append("group ").append(count.incrementAndGet()).append(": ").append('\n');
                v.getValue().forEach(p -> sb.append("  ").append(p).append('\n'));
            }
        });
        try {
            Files.write(Paths.get(RESULT_PATH), sb.toString().getBytes());
        } catch (IOException e) {
            System.out.println("ERROR: Cannot save results to a file: " + e.getMessage());
        }
    }

    /**
     * @param path Root path of the directory to find all files duplicates
     */
    public static void findDuplicates(String path) {
        printResults(runHashGeneration(generatePathsList(path)));
    }

    public static class HashCodeCallable implements Callable<HashCode> {

        private String path;

        public HashCodeCallable(String path) {
            this.path = path;
        }

        @Override
        public HashCode call() {
            try {
                return com.google.common.io.Files.asByteSource(Paths.get(path).toFile()).hash(Hashing.sha256());
            } catch (IOException ex) {
                System.out.println("WARN: Unable to get hash for file: " + ex.getMessage());
                return null;
            }
        }
    }
}