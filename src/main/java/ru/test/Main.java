package ru.test;

public class Main {

    public static void main(String[] args) {
        long m = System.currentTimeMillis();
        if (args.length < 1) {
            System.out.println("ERROR: Path in command line is required");
        } else {
            FolderScan.findDuplicates(args[0]);
            System.out.println("execution time:" + (double) (System.currentTimeMillis() - m));
        }
    }
}
