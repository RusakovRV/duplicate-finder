package ru.test;


import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class FolderScanTest {

    private final String TEST_PATH = "src\\test\\resources";
    private final String FILE_1 = "\\file1.txt";
    private final String FILE_2 = "\\file2.txt";
    private final String FILE_3 = "\\test_folder\\file3.txt";

    @Test
    public void testGeneratePathsList() {
        List<String> result = FolderScan.generatePathsList(TEST_PATH);
        assertThat(result.size()).isEqualTo(3);
    }

    @Test
    public void testRunHashGeneration() {
        List<String> filesToCalculateHash = new ArrayList<>();
        String path1 = Paths.get(TEST_PATH + FILE_1).toAbsolutePath().toString();
        String path2 = Paths.get(TEST_PATH + FILE_2).toAbsolutePath().toString();
        String path3 = Paths.get(TEST_PATH + FILE_3).toAbsolutePath().toString();
        filesToCalculateHash.add(path1);
        filesToCalculateHash.add(path2);
        filesToCalculateHash.add(path3);
        Map<String, List<String>> result = FolderScan.runHashGeneration(filesToCalculateHash);
        assertThat(result.entrySet().size()).isEqualTo(2);
        result.entrySet().forEach(v -> {
            if (v.getValue().size() == 2) {
                assertThat(v.getValue()).containsExactlyInAnyOrder(path1, path3);
            } else {
                assertThat(v.getValue()).containsExactlyInAnyOrder(path2);
            }
        });
    }

    @Test
    public void testPrintResults() {
        List<String> resultsForGroup1 = new ArrayList<>();
        resultsForGroup1.add(Paths.get(TEST_PATH + FILE_1).toAbsolutePath().toString());
        resultsForGroup1.add(Paths.get(TEST_PATH + FILE_3).toAbsolutePath().toString());
        Map<String, List<String>> resultToPrint = new HashMap<>();
        resultToPrint.put("0c1ad712dd11a219b720c6f345ac1bf3061a69377e68ac6c4021802976ae814f", resultsForGroup1);
        FolderScan.printResults(resultToPrint);
        try {
            String resultInFile = Files.readString(Paths.get(FolderScan.RESULT_PATH));
            assertThat(resultInFile).contains(resultsForGroup1.get(0));
            assertThat(resultInFile).contains(resultsForGroup1.get(1));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
